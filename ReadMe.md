# UserIDays - Register App

## Requirements

- ASP.NET Core 2.1
- Microsoft SQL Server

## Build 

- Execute `build-release.sh`

## Running the site locally

- Execute `run.sh` and navigate to your localhost
- This does a run against the project, not the DLL. Currently a bug that's being released in [Core 3.0](https://github.com/dotnet/cli/pull/7166)

## Running unit tests

- Execute `test.sh`

## Running Acceptance tests

- Execute `test-acceptance.sh`

--- 

# Thoughts and Notes

## Missing implementation

Here are the extra things I would do given more time on this project:

- Implement Integration tests to test the Data layer Get/Add methods. Their interactions are covered in Acceptance tests, but there's no guarentee that the concrete implementations are functional
- Monitoring/Logging
- Seperate the password hashing & storage to seperate releasable service. Helps release connections quickly
- Environmental Configuration
- Caching layer for existing accounts
- Look at ways of 'failing fast'
- Add redirect on successful sign up to provide a better UX
- Provide more direct feedback on errors
- Sort out the resubmission alert
- Implement password confirmation box (client side)
- Investigate and implement more accessibility, using [recognised guidence](https://www.gov.uk/service-manual/helping-people-to-use-your-service/making-your-service-accessible-an-introduction)
- Improve overall test coverage


## Client Side vs Server Side

Current the application is predominantly server side. The downside of this is there is potential for a large amount of requests coming into the server that could be served on the client side this increasing the load of the servers. Client side would allow for the user's PC to do an element of the grunt work, the server side would only have to validate the requests which should be minimal.

Personally I would likely rewrite this to decouple that front end elements away, and keep the backend to an API (likely Core Web API). It's a much cleaner solution. It does bring with it some additional concerns but it's a well documentated and typically preferred approach.

### Validation

Running some client side validation would be a prime advantage. Before sending a request to the server the user could recieve a block regarding password validation rules. If they were to circumvent these they'd still get santity checked server side regardless.

## Release

I'd look to include Environmental confirguration settings so that the passwords/logins are not checked into the source control. It also allows for the build servers to be in more direct control when deploying the application to various target environments and reduces 'if(development)' statements within the code base. As it's a cross cutting concern. Dot Net CLI provides some functionality to enable this, so it's likely I'd investigate that as a primary candidate rather than rolling my own solution.

## Testing

My approach to testing generally is to ensure that I have solid behavioral tests in place. These can act both as documentation (how the system works), but also serve as regression tests to maintain the system when large scale refactoring/decoupling occur. I tend to approach a more linear testing strategy, rather than the traditional pyramid to strike a balance between maintainable code but ensure coverage on complex functionality.

I typically red-green-refactor; driving from Acceptance level initally which should remain red until all layers down are completed ie. Top Down development.

### Acceptance

- Drive out the journey's through the system (typically BDD style).

Ideally with the acceptance tests I would be mocking out the contracts (APIs) and pointing at/creating a database connection (possibly in memory) at runtime. This can be achieved through abstraction and dependancy injection as demonstrated. This uses Microsoft's recommended approach found [here.](https://docs.microsoft.com/en-us/aspnet/core/test/integration-tests?view=aspnetcore-2.1)

### Unit

- Assist code structure/reducing smells
- Help with self documenting code aka. Developer of tomorrow
- Provide fine grained testing to maintain the testing pyramid
- Quicker feedback loop of issues with complex functionality eg. Heavy maths problems.

### Integration

- Generally contractual testing of the outer extremes of the systems. Interactions with 3rd parties, database interactions, etc

I have chosen not to write integration tests as part of this application due to time constaints; however as the Data layer retrieves it's configuration through injected functionality this could be mocked out to return a local database connection string. Data 'builders' can be used to add pre-existing users if required by the scenarios and would follow a standard Unit Test/BDD approach.

## Security

Why haven't I implemented a 'You're already registered'?

* The application could be something sensitive (medical perhaps) alerting sign up might give personal information away.
* If a hacker knows that an account exists on the system then it's potential that they could try to brute force.
    * https://www.owasp.org/index.php/Testing_for_User_Enumeration_and_Guessable_User_Account_(OWASP-AT-002)

How would I go around this? My solution would entail emailing a registration link to the desired email, upon clicking it would validate the registration with the details you entered.

Secondary to that, I would encourage the use of 2FA. Depending on the application (UX & Barrier to entry comes into play here) then I would give the user at least the option of using 2FA.

### OWASP

#### Password Security
[Troy Hunt](https://www.troyhunt.com/passwords-evolved-authentication-guidance-for-the-modern-era/)
[NIST](https://www.nist.gov/blogs/taking-measure/easy-ways-build-better-p5w0rd)
#### Password Hashing
[OWASP Link](https://www.owasp.org/index.php/Password_Storage_Cheat_Sheet)

Storing the Salt against the password to ensure there is no generic salt that, if cracked, could compromise the system. I could also add a 'pepper' though right now seemed excessive.

Although OWASP suggest other password encryption methods, given the nature of the challenge (time senstive) I chose BCrypt as it has an open source library with reasonable usage. If I was taking more time I would consider the algorithm at length as the hash is 'one way'. You could always migrate in the future by indicating which encryption method (in the db) that was used and utilise the correct hash method in the code. 

Unfortunatly the library isn't compiled against .net 4.6.1 so may cause issues. It might be possible to open a PR and compile against the correct standard.

## App Format

[12 Factor App](https://12factor.net/)

### n (three) Tier App

This app loosely follows a three tier application with seperation from view, service and data layers.

## Docker

If I were to invest some more time I would create DockerFiles to build and execute the application within containers. I'd aim to follow the core principles of the 12 Factor App and ensure that the images that are build tested, are the same as the ones being released. This means the application and tests can be ran on any machine without the need for prerequisits. Isolating the need for version controlling assets, such as DotNet Core or SQL Server and hence maintaining a more realistic testing environment.

eg. Using DockerCompose to inject environmental configuration to dictate the Database connection string. This could also be modified to inject environmental variables for a test (demonstrated below).

```yaml 
version: '3'
services:
  app:    
    image: app    
    ports:
      - "8000:80"
    environment:      
      - DBHost=http://mssql:1433
    command: dotnet test integrationTests.sln  
  mssql:
      image: "microsoft/mssql-server-linux"
      env_file:
          - mssql/variables.env
      ports:
          - "1433:1433"
    sut:   
        image: app/testing
        depends_on:
            - app
        environment:
            - ApiHost=http://app:8000
            - DBHost=http://mssql:1433
        command: dotnet test UserIDays.AcceptanceTest.sln
```

**Example DockerFile**

```sh 
FROM microsoft/dotnet:2.1-sdk AS build
WORKDIR /app

COPY UserIDays/UserIDays/UserIDays.csproj ./UserIDays/UserIDays.csproj
COPY UserIDays/UserIDays.UnitTests/UserIDays.csproj ./UserIDays.UnitTests/UserIDays.UnitTests.csproj
RUN dotnet restore ./UserIDays/

COPY ./UserIDays ./release/

WORKDIR /app/release/UserIDays
RUN dotnet publish -c Release -o out


FROM build AS testrunner
WORKDIR /app/tests
COPY UserIDays/tests/. .
RUN ls .
ENTRYPOINT ["dotnet", "test", "./UserIDays.UnitTests"]


FROM microsoft/dotnet:2.1-aspnetcore-runtime AS runtime
WORKDIR /app
COPY --from=build /app/release/UserIDays/out ./
ENTRYPOINT ["dotnet", "UserIDays.dll"]
```

An additional change I would like to make would be to add configuration as part of environmental variables and template the configuration, this along with some other modifications, would help align to the 'Twelve Factor App' methodology.

## Shell Scripts

The Shell scripts are an integral part of keeping integrity amongst the various environments that this code might be executed, and reducing the complexity of the build/deploy pipelines. Currently the shell scripts purpose are really only for execution on a development machine/build agent. What it provides though is a source controlled, consistent method to build, test and run the application.

Ultimately it would be great to implement multiple Environment based DockerFiles and scripting (dev, staging, production) which could help to improve:
- Security - By removing the need for keys/passwords to be baked into environmental variables
- Remove Fluff - Remove any extra DLLs/Configuration that are not required for a Production environment. This can also help with security
- Logging/Debugging - Having different granularity and access/alerting to various parts of the systems