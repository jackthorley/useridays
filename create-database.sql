CREATE TABLE [User] (
	[Id] int NOT NULL IDENTITY (1,1) PRIMARY KEY,
	[Email] varchar(255) NOT NULL,
	[Password] varchar(255) NOT NULL,
	[Salt] varchar(255) NOT NULL,
	[DateRegistered] datetime DEFAULT GETDATE()
);

CREATE LOGIN UserIDaysWeb WITH PASSWORD = 'dG9wa2VraW5ndG9uYmFpbnM=';
CREATE USER UserIDaysWeb FOR LOGIN UserIDaysWeb;
GRANT SELECT ON Users TO UserIDaysWeb
GRANT INSERT ON Users TO UserIDaysWeb