﻿namespace UserIDays.UnitTests.Controller
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Controllers;
    using Models.View;
    using Microsoft.AspNetCore.Mvc;
    using Models;
    using Models.Converters;
    using Models.UserDetail;
    using Moq;
    using UserIDays.Services.User;
    using Xunit;

    public class GivenAValidUserDetailsWhenAddingValidUser
    {
        private readonly IActionResult _result;
        private readonly Mock<IConverter<UserDetailsViewModel, ModelResult<UserDetail, UserConvertionStatus>>> _converter;
        private readonly UserDetailsViewModel _userDetailsViewModel;
        private readonly ModelResult<UserDetail, UserConvertionStatus> _convertionResult;
        private readonly Mock<IUserService> _userService;

        public GivenAValidUserDetailsWhenAddingValidUser()
        {
            _userDetailsViewModel = new UserDetailsViewModel();
            _convertionResult = new ModelResult<UserDetail, UserConvertionStatus>(new UserDetail("username", "password"), UserConvertionStatus.Valid);

            _converter = new Mock<IConverter<UserDetailsViewModel, ModelResult<UserDetail, UserConvertionStatus>>>();
            _converter.Setup(c => c.Convert(It.IsAny<UserDetailsViewModel>()))
                      .Returns(Task.FromResult(_convertionResult));

            _userService = new Mock<IUserService>();

            var controller = new RegisterController(_converter.Object, _userService.Object);
            _result = controller.Index(_userDetailsViewModel).Result;
        }

        [Fact]
        public void ThenTheUserDetailsModelIsConverted()
        {
            _converter.Verify(c => c.Convert(_userDetailsViewModel));
        }

        [Fact]
        public void ThenTheUserDetailsArePassedToTheUserService()
        {
            _userService.Verify(us => us.Create(_convertionResult.Result));
        }

        [Fact]
        public void ThenTheViewIsReturned()
        {
            Assert.IsType<ViewResult>(_result);
        }
    }
}
