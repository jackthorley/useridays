﻿namespace UserIDays.UnitTests.Controller
{
    using System.Threading.Tasks;
    using Controllers;
    using Microsoft.AspNetCore.Mvc;
    using Models;
    using Models.Converters;
    using Models.UserDetail;
    using Models.View;
    using Moq;
    using Xunit;

    public class GivenAValidUserDetailsWhenAddingInvalidUser
    {
        private readonly IActionResult _result;
        private readonly Mock<IConverter<UserDetailsViewModel, ModelResult<UserDetail, UserConvertionStatus>>> _converter;
        private readonly UserDetailsViewModel _userDetailsViewModel;
        private readonly string _username;

        public GivenAValidUserDetailsWhenAddingInvalidUser()
        {
            _username = "username";
            _userDetailsViewModel = new UserDetailsViewModel {Email = _username, Password = "aasdasd"};

            var convertionResult = new ModelResult<UserDetail, UserConvertionStatus>(null, UserConvertionStatus.Invalid);

            _converter = new Mock<IConverter<UserDetailsViewModel, ModelResult<UserDetail, UserConvertionStatus>>>();
            _converter.Setup(c => c.Convert(It.IsAny<UserDetailsViewModel>()))
                      .Returns(Task.FromResult(convertionResult));
            
            var controller = new RegisterController(_converter.Object, null);
            _result = controller.Index(_userDetailsViewModel).Result;
        }

        [Fact]
        public void ThenTheUserDetailsModelIsConverted()
        {
            _converter.Verify(c => c.Convert(_userDetailsViewModel));
        }

        [Fact]
        public void ThenTheViewIsReturnedWithTheEmail()
        {
            var viewResult = Assert.IsType<ViewResult>(_result);
            var model = Assert.IsAssignableFrom<UserDetailsViewModel>(viewResult.ViewData.Model);

            Assert.Equal(_username, model.Email);
            Assert.Null(model.Password);
        }

    }
}