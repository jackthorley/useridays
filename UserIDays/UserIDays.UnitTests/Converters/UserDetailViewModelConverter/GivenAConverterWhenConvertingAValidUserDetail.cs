﻿namespace UserIDays.UnitTests.Converters.UserDetailViewModelConverter
{
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;
    using FluentValidation;
    using FluentValidation.Results;
    using Models;
    using Models.Converters;
    using Models.UserDetail;
    using Models.View;
    using Moq;
    using Xunit;

    public class GivenAUserDetailConverter
    {
        private ModelResult<UserDetail, UserConvertionStatus> _result;

        [Fact]
        public void WhenUserDetailIsValidThenReturnUserDetail()
        {
            var userDetailsViewModel = new UserDetailsViewModel {Email = "testhahah", Password = "whatever"};

            var validator = new Mock<IValidator>();
            validator.Setup(v => v.ValidateAsync(It.IsAny<object>(), CancellationToken.None))
                .Returns(Task.FromResult(new ValidationResult()));

            var converter = new UserDetailsViewModelConverter(validator.Object);
            _result = converter.Convert(userDetailsViewModel).Result;

            Assert.Equal(userDetailsViewModel.Email, _result.Result.Email);
            Assert.Equal(userDetailsViewModel.Password, _result.Result.Password);
            Assert.Equal(UserConvertionStatus.Valid, _result.Status);
        }

        [Fact]
        public void WhenUserDetailIsInvalidThenReturnUserDetail()
        {
            var userDetailsViewModel = new UserDetailsViewModel { Email = "testhahah", Password = "whatever" };

            var validator = new Mock<IValidator>();
            validator.Setup(v => v.ValidateAsync(It.IsAny<object>(), CancellationToken.None))
                     .Returns(Task.FromResult(new ValidationResult(new List<ValidationFailure> {new ValidationFailure("Password", "Something goofed")})));

            var converter = new UserDetailsViewModelConverter(validator.Object);
            _result = converter.Convert(userDetailsViewModel).Result;

            Assert.Equal(UserConvertionStatus.Invalid, _result.Status);
            Assert.IsType<NullUserDetail>(_result.Result);
        }

    }
}
