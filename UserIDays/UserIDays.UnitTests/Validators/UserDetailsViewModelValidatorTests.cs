﻿namespace UserIDays.UnitTests.Validators
{
    using System.Collections.Generic;
    using FluentValidation.TestHelper;
    using UserIDays.Validators;
    using Xunit;

    public class UserDetailsViewModelValidatorTests
    {
        [Theory]
        [MemberData(nameof(InvalidPasswords))]
        public void GivenInvalidPasswords(string password)
        {
            var userDetailsViewModelValidator = new UserDetailsViewModelValidator();
            userDetailsViewModelValidator.ShouldHaveValidationErrorFor(u => u.Password, password);
        }

        [Theory]
        [MemberData(nameof(ValidPasswords))]
        public void GivenValidPasswords(string password)
        {
            var userDetailsViewModelValidator = new UserDetailsViewModelValidator();
            userDetailsViewModelValidator.ShouldNotHaveValidationErrorFor(u => u.Password, password);
        }

        public static IEnumerable<object[]> InvalidPasswords()
        {
            var invalidPasswords = new List<object[]>
            {
                new object[] {new string('a', 129)},
                new object[] {"Under10!"},
                new object[] {"0123456789<"},
                new object[] {"0123456789>"},
                new object[] {"01234><56789"}
            };

            return invalidPasswords;
        }

        public static IEnumerable<object[]> ValidPasswords()
        {
            var validPasswords = new List<object[]>
            {
                new object[] {new string('a', 128)},
                new object[] {new string('a', 10)},
                new object[] {"!234%6789a"}
            };

            return validPasswords;
        }
    }
}