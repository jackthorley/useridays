﻿namespace UserIDays.UnitTests.Services.PasswordHash
{
    using Moq;
    using UserIDays.Services.PasswordHash;
    using Xunit;

    public class PasswordHashTests
    {
        [Fact]
        public void GivenAPasswordWhenGeneratingItsHash()
        {
            var salt = "$2a$10$ooJsLpW.ywkyxWSWBPv4LO";

            var saltFactory = new Mock<ISaltFactory>();
            saltFactory.Setup(s => s.GenerateSalt()).Returns(salt);

            var service = new PasswordHashService(saltFactory.Object);
            var hash = service.Hash("12345").Result;

            Assert.Equal(salt, hash.Salt);
            Assert.Equal("$2a$10$ooJsLpW.ywkyxWSWBPv4LOt.CNlVinUx4DrfgroDpqP8kumtjBocC", hash.HashedPassword);
        }
    }
}
