﻿namespace UserIDays.AcceptanceTests.Register
{
    using System.Collections.Generic;
    using System.Net;
    using System.Net.Http;
    using System.Threading.Tasks;
    using AngleSharp.Dom.Html;
    using Data.User;
    using Helpers;
    using Microsoft.AspNetCore.Mvc.Testing;
    using Microsoft.AspNetCore.TestHost;
    using Microsoft.Extensions.DependencyInjection;
    using Stubs;
    using Xunit;

    public class GivenARegistrationWhenTheUserPasswordIsMissing : IClassFixture<WebAppFactory<Startup>>
    {
        private readonly HttpClient _client;
        private readonly UserAddCommandStub _userAddCommandStub;

        public GivenARegistrationWhenTheUserPasswordIsMissing(WebAppFactory<Startup> factory)
        {
            _userAddCommandStub = new UserAddCommandStub();

            _client = factory.WithWebHostBuilder(builder =>
            {
                builder.ConfigureTestServices(services =>
                {
                    services.AddSingleton<IUserGetCommand>(provider => new UserGetCommandStub());
                    services.AddSingleton<IUserAddCommand>(provider => _userAddCommandStub);
                });

            }).CreateClient(new WebApplicationFactoryClientOptions
            {
                AllowAutoRedirect = true
            });
        }

        [Fact]
        public async Task ThenAUserIsNotCreatedAndTheModelIsInvalid()
        {
            var registrationPage = await _client.GetAsync("/");
            var content = await HtmlHelpers.GetDocumentAsync(registrationPage);

            var response = await _client.SendAsync((IHtmlFormElement)content.QuerySelector("form[id='frmRegister']"),
                                                   (IHtmlInputElement)content.QuerySelector("input[id='btnRegister']"),
                                                   new Dictionary<string, string>
                                                   {
                                                       ["Email"] = "test@testdomain.com"
                                                   });

            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            Assert.Empty(_userAddCommandStub.InsertedCredentials);
        }
    }
}