﻿namespace UserIDays.AcceptanceTests.Register
{
    using System.Collections.Generic;
    using System.Net;
    using System.Net.Http;
    using System.Threading.Tasks;
    using AngleSharp.Dom.Html;
    using Data.User;
    using Helpers;
    using Microsoft.AspNetCore.Mvc.Testing;
    using Microsoft.AspNetCore.TestHost;
    using Microsoft.Extensions.DependencyInjection;
    using Stubs;
    using Services;
    using Services.PasswordHash;
    using Xunit;

    public class GivenARegistrationWhenTheUserIsValidAndDoesntExistAlready : IClassFixture<WebAppFactory<Startup>>
    {
        private readonly HttpClient _client;
        private readonly UserAddCommandStub _userAddCommand;
        private readonly EmailServiceStub _emailService;
        private readonly string _salt;

        public GivenARegistrationWhenTheUserIsValidAndDoesntExistAlready(WebAppFactory<Startup> factory)
        {
            _salt = "saltymcsaltface";

            _userAddCommand = new UserAddCommandStub();
            _emailService = new EmailServiceStub();
            var userGetCommandStub = new UserGetCommandStub();
            var passwordHashServiceStub = new PasswordHashServiceStub(_salt);

            _client = factory.WithWebHostBuilder(builder =>
            {
                builder.ConfigureTestServices(services =>
                {
                    services.AddSingleton<IUserGetCommand>(provider => userGetCommandStub);
                    services.AddSingleton<IUserAddCommand>(provider => _userAddCommand);
                    services.AddSingleton<IEmailService>(provider => _emailService);
                    services.AddSingleton<IPasswordHashService>(provider => passwordHashServiceStub);
                });

            }).CreateClient(new WebApplicationFactoryClientOptions
            {
                AllowAutoRedirect = true
            });
        }

        [Fact]
        public async Task ThenAUserIsCreated()
        {
            var registrationPage = await _client.GetAsync("/");
            var content = await HtmlHelpers.GetDocumentAsync(registrationPage);

            var password = "Asdasd1233";
            var email = "test@testdomain.com";

            var response = await _client.SendAsync((IHtmlFormElement) content.QuerySelector("form[id='frmRegister']"),
                                                   (IHtmlInputElement) content.QuerySelector("input[id='btnRegister']"),
                                                   new Dictionary<string, string>
                                                   {
                                                       ["Password"] = password,
                                                       ["Email"] = email
                                                   });

            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            Assert.Single(_userAddCommand.InsertedCredentials);

            Assert.Equal(email, _userAddCommand.InsertedCredentials[0].Email);
            Assert.Equal(password, _userAddCommand.InsertedCredentials[0].Password);
            Assert.Equal(_salt, _userAddCommand.InsertedCredentials[0].Salt);

            Assert.Single(_emailService.SentEmails);
            Assert.Equal("Verification", _emailService.SentEmails[0].EmailTag);
            Assert.Equal(email, _emailService.SentEmails[0].Recipient);
        }
    }
}
