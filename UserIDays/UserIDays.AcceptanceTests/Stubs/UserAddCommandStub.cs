﻿namespace UserIDays.AcceptanceTests.Stubs
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Data.User;

    internal class UserAddCommandStub : IUserAddCommand
    {
        public List<UserCredentials> InsertedCredentials { get; }

        public UserAddCommandStub()
        {
            InsertedCredentials = new List<UserCredentials>();
        }

        public Task Add(string email, string password, string salt)
        {
            InsertedCredentials.Add(new UserCredentials(email, password, salt));
            return Task.CompletedTask;
        }
    }
}
