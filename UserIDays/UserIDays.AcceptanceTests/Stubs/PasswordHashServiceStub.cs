namespace UserIDays.AcceptanceTests.Stubs
{
    using System.Threading.Tasks;
    using Models.Password;
    using Services.PasswordHash;

    internal class PasswordHashServiceStub : IPasswordHashService
    {
        private readonly string _salt;

        public PasswordHashServiceStub(string salt)
        {
            _salt = salt;
        }

        public Task<PasswordHash> Hash(string password)
        {
            return Task.FromResult(new PasswordHash(_salt, password));
        }
    }
}