﻿namespace UserIDays.AcceptanceTests.Stubs
{
    internal class EmailServiceProcess
    {
        public string EmailTag { get; }
        public string Recipient { get; }

        public EmailServiceProcess(string emailTag, string recipient)
        {
            EmailTag = emailTag;
            Recipient = recipient;
        }
    }
}