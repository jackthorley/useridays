namespace UserIDays.AcceptanceTests.Stubs
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Services;

    internal class EmailServiceStub : IEmailService
    {
        public EmailServiceStub()
        {
            SentEmails = new List<EmailServiceProcess>();
        }

        public List<EmailServiceProcess> SentEmails { get; }

        public Task sendPayload(string emailTag, string recipient)
        {
            SentEmails.Add(new EmailServiceProcess(emailTag, recipient));
            return Task.CompletedTask;
        }
    }
}