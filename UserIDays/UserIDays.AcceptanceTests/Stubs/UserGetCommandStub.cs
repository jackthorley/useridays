﻿namespace UserIDays.AcceptanceTests.Stubs
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Data.User;

    internal class UserGetCommandStub : IUserGetCommand
    {
        private readonly bool _exists;

        public List<UserCredentials> InsertedCredentials { get; }

        public UserGetCommandStub(bool exists = false)
        {
            _exists = exists;
            InsertedCredentials = new List<UserCredentials>();
        }

        public Task<bool> Get(string email)
        {
            return Task.FromResult(_exists);
        }
    }
}