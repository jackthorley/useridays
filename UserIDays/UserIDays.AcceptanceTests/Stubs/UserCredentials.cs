﻿namespace UserIDays.AcceptanceTests.Stubs
{
    internal class UserCredentials
    {
        public string Email { get; }
        public string Password { get; }
        public string Salt { get; }

        public UserCredentials(string email, string password, string salt)
        {
            Email = email;
            Password = password;
            Salt = salt;
        }
    }
}