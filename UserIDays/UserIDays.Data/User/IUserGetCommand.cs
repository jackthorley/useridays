﻿namespace UserIDays.Data.User
{
    using System.Threading.Tasks;

    public interface IUserGetCommand
    {
        Task<bool> Get(string email);
    }
}