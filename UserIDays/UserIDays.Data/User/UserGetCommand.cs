﻿namespace UserIDays.Data.User
{
    using System.Threading.Tasks;
    using Microsoft.Extensions.Configuration;

    internal class UserGetCommand : IUserGetCommand
    {
        private readonly IConnectionFactory _connectionFactory;
        private readonly string _connectionString;

        private const string SQL = @"SELECT COUNT(1) FROM [User] WHERE [Email] = @Email";

        public UserGetCommand(IConnectionFactory connectionFactory, IConfiguration configuration)
        {
            _connectionFactory = connectionFactory;
            _connectionString = configuration["Database:ConnectionString"];
        }

        public Task<bool> Get(string email)
        {
            using (var connection = _connectionFactory.CreateConnection(_connectionString))
            {
                connection.Open();

                /* Using 'CreateCommand off the interface is more simplistic for mocking,
                 * however its interface is much worse with creating parameters.
                 * I'd look to create potentially a proxy to SqlConnection or a SqlCommand factory
                 * to allow for a better usage/more readable code 
                 */
                using (var cmd = connection.CreateCommand())
                {
                    cmd.CommandText = SQL;

                    var paramEmail = cmd.CreateParameter();

                    paramEmail.ParameterName = "Email";
                    paramEmail.Value = email;

                    cmd.Parameters.Add(paramEmail);

                    var rows = (int) cmd.ExecuteScalar();

                    return Task.FromResult(rows >= 1);
                }

            }
        }
    }
}