﻿namespace UserIDays.Data.User
{
    using System.Data.SqlClient;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Configuration;

    internal class UserAddCommand : IUserAddCommand
    {
        private readonly IConnectionFactory _connectionFactory;
        private readonly string _connectionString;

        private const string SQL = @"INSERT INTO [User] (Email, Password, Salt) 
                                   VALUES (@Email, @Password, @Salt)";

        public UserAddCommand(IConnectionFactory connectionFactory, IConfiguration configuration)
        {
            _connectionFactory = connectionFactory;
            _connectionString = configuration["Database:ConnectionString"];
        }

        public Task Add(string email, string password, string salt)
        {
            using (var connection = _connectionFactory.CreateConnection(_connectionString))
            {
                connection.Open();

                /* Using 'CreateCommand off the interface is more simplistic for mocking,
                 * however its interface is much worse with creating parameters.
                 * I'd look to create potentially a proxy to SqlConnection or a SqlCommand factory
                 * to allow for a better usage/more readable code 
                 */
                using (var cmd = connection.CreateCommand())
                {
                    cmd.CommandText = SQL;

                    var paramPassword = cmd.CreateParameter();
                    var paramSalt = cmd.CreateParameter();
                    var paramEmail = cmd.CreateParameter();

                    paramPassword.ParameterName = "Password";
                    paramPassword.Value = password;

                    paramSalt.ParameterName = "Salt";
                    paramSalt.Value = salt;

                    paramEmail.ParameterName = "Email";
                    paramEmail.Value = email;

                    cmd.Parameters.Add(paramPassword);
                    cmd.Parameters.Add(paramSalt);
                    cmd.Parameters.Add(paramEmail);

                   cmd.ExecuteNonQuery();
                }

            }

            return Task.CompletedTask;
        }

    }
}