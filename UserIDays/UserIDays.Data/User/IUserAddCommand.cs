﻿namespace UserIDays.Data.User
{
    using System.Threading.Tasks;

    public interface IUserAddCommand
    {
        Task Add(string email, string password, string salt);
    }
}
