﻿namespace UserIDays.Data
{
    using System.Data;
    using System.Data.SqlClient;

    public class SQLServerConnectionFactory : IConnectionFactory
    {
        public IDbConnection CreateConnection(string connectionString)
        {
            return new SqlConnection(connectionString);
        }
    }
}