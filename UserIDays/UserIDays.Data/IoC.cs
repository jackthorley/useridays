﻿namespace UserIDays.Data
{
    using Microsoft.Extensions.DependencyInjection;
    using User;

    public static class IoC
    {
        public static IServiceCollection AddDataLayer(this IServiceCollection services)
        {
            services.AddTransient<IUserAddCommand, UserAddCommand>();
            services.AddTransient<IUserGetCommand, UserGetCommand>();
            services.AddTransient<IConnectionFactory, SQLServerConnectionFactory>();
            return services;
        }
    }
}
