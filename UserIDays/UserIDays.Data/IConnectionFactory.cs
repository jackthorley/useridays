﻿namespace UserIDays.Data
{
    using System.Data;

    public interface IConnectionFactory
    {
        IDbConnection CreateConnection(string connectionString);
    }
}
