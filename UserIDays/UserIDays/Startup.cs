﻿namespace UserIDays
{
    using Data;
    using Data.User;
    using FluentValidation;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Models;
    using Models.Converters;
    using Models.UserDetail;
    using Models.View;
    using Services;
    using Services.PasswordHash;
    using Services.User;
    using Validators;

    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddDataLayer();
            services.AddTransient<IValidator, UserDetailsViewModelValidator>();
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IEmailService, EmailFakeService>();
            services.AddTransient<IPasswordHashService, PasswordHashService>();
            services.AddTransient<ISaltFactory, SaltFactory>();
            services.AddTransient<IConverter<UserDetailsViewModel, ModelResult<UserDetail, UserConvertionStatus>>, UserDetailsViewModelConverter>();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Register}/{action=Index}/{id?}");
            });
        }
    }
}
