﻿namespace UserIDays.Validators
{
    using FluentValidation;
    using Models.View;

    internal class UserDetailsViewModelValidator : AbstractValidator<UserDetailsViewModel>
    {
        public UserDetailsViewModelValidator()
        {
            RuleFor(x => x.Password).MinimumLength(10);
            RuleFor(x => x.Password).MaximumLength(128);
            RuleFor(x => x.Password).Must(NotContainAngleBrackets);
        }

        internal bool NotContainAngleBrackets(string entry)
        {
            return !(entry.Contains('<') || entry.Contains('>'));
        }
    }
}