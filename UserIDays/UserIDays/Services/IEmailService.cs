﻿namespace UserIDays.Services
{
    using System.Threading.Tasks;

    internal interface IEmailService
    {
        Task sendPayload(string emailTag, string recipient);
    }
}