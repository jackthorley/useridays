﻿namespace UserIDays.Services.User
{
    using System.Threading.Tasks;
    using Models.UserDetail;

    public interface IUserService
    {
        Task Create(UserDetail userDetail);
    }
}