﻿namespace UserIDays.Services.User
{
    using Models.UserDetail;
    using System.Threading.Tasks;
    using Data.User;
    using PasswordHash;

    internal class UserService : IUserService
    {
        private readonly IPasswordHashService _passwordHashService;
        private readonly IUserGetCommand _userGetCommand;
        private readonly IUserAddCommand _userAddCommand;
        private readonly IEmailService _emailService;

        public UserService(IPasswordHashService passwordHashService,
                           IUserGetCommand userGetCommand,
                           IUserAddCommand userAddCommand,
                           IEmailService emailService)
        {
            _passwordHashService = passwordHashService;
            _userGetCommand = userGetCommand;
            _userAddCommand = userAddCommand;
            _emailService = emailService;
        }

        public async Task Create(UserDetail userDetail)
        {
            var userExists = await _userGetCommand.Get(userDetail.Email);

            if (userExists)
            {
                //Trigger you're already a user - do you want a password reset?
                await _emailService.sendPayload("Reset", userDetail.Email);
                return;
            }

            var passwordHash = await _passwordHashService.Hash(userDetail.Password);
            await _userAddCommand.Add(userDetail.Email, passwordHash.HashedPassword, passwordHash.Salt);

            //Trigger verification Email
            //Probably grab the tag from some centralised configuration location, baked in magic values are pretty bad.
            //Having it centralised would allow for potential A/B, or version control.
            await _emailService.sendPayload("Verification", userDetail.Email);
        }
    }
}