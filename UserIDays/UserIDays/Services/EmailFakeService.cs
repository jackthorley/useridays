﻿namespace UserIDays.Services
{
    using System.Threading.Tasks;

    internal class EmailFakeService : IEmailService
    {
        public Task sendPayload(string emailTag, string recipient)
        {
            //Grab Email Tag Content from centralised data source
            return Task.CompletedTask;
        }
    }
}