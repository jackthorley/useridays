﻿namespace UserIDays.Services.PasswordHash
{
    internal interface ISaltFactory
    {
        string GenerateSalt();
    }
}