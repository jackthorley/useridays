﻿namespace UserIDays.Services.PasswordHash
{
    using System.Threading.Tasks;
    using Models.Password;

    internal interface IPasswordHashService
    {
        Task<PasswordHash> Hash(string password);
    }
}