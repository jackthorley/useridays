﻿namespace UserIDays.Services.PasswordHash
{
    using System.Threading.Tasks;
    using DevOne.Security.Cryptography.BCrypt;
    using Models.Password;

    internal class PasswordHashService : IPasswordHashService
    {
        private readonly ISaltFactory _saltFactory;

        public PasswordHashService(ISaltFactory saltFactory)
        {
            _saltFactory = saltFactory;
        }

        public Task<PasswordHash> Hash(string password)
        {
            var salt = _saltFactory.GenerateSalt();
            var hashedPassword = BCryptHelper.HashPassword(password, salt);

            return Task.FromResult(new PasswordHash(salt, hashedPassword));
        }
    }
}