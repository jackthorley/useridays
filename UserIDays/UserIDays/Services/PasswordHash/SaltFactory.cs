﻿namespace UserIDays.Services.PasswordHash
{
    using DevOne.Security.Cryptography.BCrypt;

    internal class SaltFactory : ISaltFactory
    {
        public string GenerateSalt()
        {
            return BCryptHelper.GenerateSalt();
        }
    }
}