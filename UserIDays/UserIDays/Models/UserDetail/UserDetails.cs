﻿namespace UserIDays.Models.UserDetail
{
    using System;

    public class UserDetail
    {
        public string Email { get; }
        public string Password { get; }

        public UserDetail(string email, string password)
        {
            Email = email;
            Password = password;
        }

        public static readonly NullUserDetail Null = NullUserDetail;

        private static NullUserDetail NullUserDetail
        {
            get
            {
                return new NullUserDetail();
            }
        }
    }

    public class NullUserDetail : UserDetail
    {
        public NullUserDetail() : base(String.Empty, String.Empty)
        {
        }
    }
}