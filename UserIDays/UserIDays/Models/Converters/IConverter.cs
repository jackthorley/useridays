﻿namespace UserIDays.Models.Converters
{
    using System.Threading.Tasks;

    public interface IConverter<in T, T1> where T1 : IModelResult
    {
        Task<T1> Convert(T input);
    }
}