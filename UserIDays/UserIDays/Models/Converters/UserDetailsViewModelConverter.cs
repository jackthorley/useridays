﻿namespace UserIDays.Models.Converters
{
    using System.Threading.Tasks;
    using FluentValidation;
    using UserDetail;
    using View;

    internal class UserDetailsViewModelConverter : IConverter<UserDetailsViewModel, ModelResult<UserDetail, UserConvertionStatus>>
    {
        private readonly IValidator _userDetailsValidator;

        public UserDetailsViewModelConverter(IValidator userDetailsValidator)
        {
            _userDetailsValidator = userDetailsValidator;
        }

        public async Task<ModelResult<UserDetail, UserConvertionStatus>> Convert(UserDetailsViewModel input)
        {
            var validationResult = await _userDetailsValidator.ValidateAsync(input);

            if(!validationResult.IsValid)
                return new ModelResult<UserDetail, UserConvertionStatus>(UserDetail.Null, UserConvertionStatus.Invalid);

            var userDetail = new UserDetail(input.Email, input.Password);

            return new ModelResult<UserDetail, UserConvertionStatus>(userDetail, UserConvertionStatus.Valid); ;
        }
    }
}