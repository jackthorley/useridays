﻿namespace UserIDays.Models
{
    public class ModelResult<T, T1> : IModelResult where T : class
    {
        public T Result { get; }
        public T1 Status { get; }

        public ModelResult(T result, T1 status)
        {
            Result = result;
            Status = status;
        }
    }
}