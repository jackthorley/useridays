﻿namespace UserIDays.Models.View
{
    using System.ComponentModel.DataAnnotations;
    using Microsoft.AspNetCore.Mvc;

    public class UserDetailsViewModel
    {
        [FromForm]
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [FromForm]
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}