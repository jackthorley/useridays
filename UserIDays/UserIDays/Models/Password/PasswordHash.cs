﻿namespace UserIDays.Models.Password
{
    public class PasswordHash
    {
        public string Salt { get; }
        public string HashedPassword { get; }
        
        public PasswordHash(string salt, string hashedPassword)
        {
            Salt = salt;
            HashedPassword = hashedPassword;
        }
    }
}