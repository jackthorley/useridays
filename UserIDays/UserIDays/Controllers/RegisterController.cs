﻿namespace UserIDays.Controllers
{
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Models;
    using Models.Converters;
    using Models.UserDetail;
    using Models.View;
    using Services.User;

    public class RegisterController : Controller
    {
        private readonly IConverter<UserDetailsViewModel, ModelResult<UserDetail, UserConvertionStatus>> _modelConverter;
        private IUserService _userService;


        public RegisterController(IConverter<UserDetailsViewModel, ModelResult<UserDetail, UserConvertionStatus>> modelConverter,
                                  IUserService userService)
        {
            _userService = userService;
            _modelConverter = modelConverter;
        }

        public async Task<IActionResult> Index()
        {
            return View();
        }

        [HttpPost, ValidateAntiForgeryToken]
        [AllowAnonymous]
        public async Task<IActionResult> Index(UserDetailsViewModel userDetailsViewModel)
        {
            if (!ModelState.IsValid)
            {
                userDetailsViewModel.Password = null;

                if(ModelState["Password"].Errors.Count > 0)
                    ModelState.AddModelError("PasswordError", "Invalid password");

                if (ModelState["Email"].Errors.Count > 0)
                    ModelState.AddModelError("EmailError", "Invalid password");

                return View(userDetailsViewModel);
            }

            var userDetailsResult = await _modelConverter.Convert(userDetailsViewModel);

            if (userDetailsResult.Status == UserConvertionStatus.Invalid)
            {
                userDetailsViewModel.Password = null;

                ModelState.AddModelError("PasswordError", "Invalid password");

                return View(userDetailsViewModel);
            }

            await _userService.Create(userDetailsResult.Result);

            return View();
        }
    }
}
